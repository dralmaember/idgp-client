#!/usr/bin/env python3

import readline as rl

from commands import Shell

def handle_cmd(cmd):
	cmd = cmd.split(" ", 1)
	if len(cmd) == 1:
		cmd.append("")
	try:
		commands[cmd[0]](cmd[1])
	except IndexError:
		print("error: command not found: %s"%(cmd[0]))

Shell().cmdloop()
