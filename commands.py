import sys, cmd

import conn

class Shell(cmd.Cmd):
	intro = "Welcome! Type \"?\" for a list of commands or \"set\" to explore the available options."
	prompt = "% "
	file = None

	def __init__(self):
		cmd.Cmd.__init__(self)
		self.opts = {
			"tls": "off",
			"listpagelines": "10",
			"editor": "nano",
			"addr": None,
			"port": None}

	def do_set(self, cmdline):
		"Set an option: set <option> <value>, or read an option: set <option>, or list all options: set"
		args = cmdline.split(" ", 2)
		if cmdline == "":
			for k, v in self.opts.items():
				print("%s = %s"%(k, v))
		elif len(args) == 1:
			try:
				print("%s = %s"%(args[0], self.opts[args[0]]))
			except IndexError:
				print("%s not found"%(args[1]))
		elif len(args) == 2:
			self.opts[args[0]] = args[1]

	def do_quit(self, cmdline):
		"Quit the program"
		sys.exit(0)

	def do_get(self, cmdline):
		"""Get a post by id. Usage: get <group> <id>. It requires that the options addr and port are set. Set tls to "on" if you want to connect through TLS"""
		if self.opts["addr"] is None or self.opts["port"] is None:
			print("Port and address must be set!")
			return
		try:
			port = int(self.opts["port"])
		except ValueError:
			print("Invalid port")
			return

		cmdline = cmdline.split(" ")
		if len(cmdline) != 2:
			print("Expected 2 arguments. You provided %s"%(len(cmdline)))
			return

		try:
			postid = int(cmdline[1])
			group = cmdline[0]
		except ValueError:
			print("Invalid arguments!")
			return

		try:
			con = conn.Conn(self.opts["addr"], port, self.opts["tls"] == "on")
			con.connect()
			post = con.get(group, postid)
			con.disconnect()
		except conn.ConnError as e:
			print("Error: %s"%(e.desc))
			return
		except Exception as e:
			print("Misc. uncaught error: %s"%e)
			return

		try:
			listpagelines = int(self.opts["listpagelines"])
		except ValueError:
			listpagelines = len(posts)

		lines = post.split("\n")
		i = 0
		while i < len(lines):
			for j in range(listpagelines):
				if i + j >= len(lines):
					break
				print(lines[i + j])
			i += listpagelines
			if input("Press enter for more, or q and enter to stop listing") == "q":
				break

	def do_lg(self, cmdline):
		"List available groups. The options addr and port must be set. Set tls to \"on\" if you want to connect through TLS"
		if self.opts["addr"] is None or self.opts["port"] is None:
			print("Port and address must be set!")
			return
		try:
			port = int(self.opts["port"])
		except ValueError:
			print("Invalid port")
			return

		try:
			con = conn.Conn(self.opts["addr"], port, self.opts["tls"] == "on")
			con.connect()
			groups = con.listGroups()
			con.disconnect()
		except conn.ConnError as e:
			print("Error: %s"%(e.desc))
			return
		except Exception as e:
			print("Misc. uncaught error: %s"%e)
			return

		try:
			listpagelines = int(self.opts["listpagelines"])
		except ValueError:
			listpagelines = len(groups)

		groups = groups.split("\n")
		i = 0
		while i < len(groups):
			for j in range(listpagelines):
				if i + j >= len(groups):
					break
				print(groups[i + j])
			i += listpagelines
			if input("Press enter for more, or q and enter to stop listing") == "q":
				break

	def do_list(self, cmdline):
		"List posts in a group. Syntax: list <group> <offset> <count>. If the offset is \"-\", it will begin\
 with the latest post. The options addr and port must be set. Set tls to \"on\" if you want to connect through TLS."
		if self.opts["addr"] is None or self.opts["port"] is None:
			print("Port and address must be set!")
			return
		try:
			port = int(self.opts["port"])
		except ValueError:
			print("Invalid port")
			return

		cmdline = cmdline.split(" ")
		if len(cmdline) != 3:
			print("Expected 3 arguments. You provided %s"%(len(cmdline)))
			return

		if cmdline[1] == "-":
			cmdline[1] = "0"

		try:
			offset, count = int(cmdline[1]), int(cmdline[2])
			group = cmdline[0]
		except ValueError:
			print("Invalid arguments!")
			return

		try:
			con = conn.Conn(self.opts["addr"], port, self.opts["tls"] == "on")
			con.connect()
			posts = con.list(group, offset, count)
			con.disconnect()
		except conn.ConnError as e:
			print("Error: %s"%(e.desc))
			return
		except Exception as e:
			print("Misc. uncaught error: %s"%e)
			return

		try:
			listpagelines = int(self.opts["listpagelines"])
		except ValueError:
			listpagelines = len(posts)

		posts = posts.split("\n")
		i = 0
		while i < len(posts):
			for j in range(listpagelines):
				if i + j >= len(posts):
					break
				print(posts[i + j])
			i += listpagelines
			if input("Press enter for more, or q and enter to stop listing") == "q":
				break

	def do_post(self, cmdline):
		"Create a post. It will interactively prompt for data. Set tls to \"on\" if you want to connect through TLS."
		if self.opts["addr"] is None or self.opts["port"] is None:
			print("Port and address must be set!")
			return
		try:
			port = int(self.opts["port"])
		except ValueError:
			print("Invalid port")
			return

		postSubject = input("Subject: ")
		postGroup = input("Group: ")
		postAuthor = input("Author: ")

		try:
			import tempfile, os
			from subprocess import call

			tf = tempfile.NamedTemporaryFile(suffix=".tmp", delete = False)
			tf.write(b"<Insert you post contents here>")
			tf.close()

			call([self.opts["editor"], tf.name])

			f = open(tf.name, "r")
			body = f.read().encode("utf-8")
			f.close()

			os.unlink(tf.name)
		except Exception as e:
			print("Failed to edit post: %s"%e)

		try:
			con = conn.Conn(self.opts["addr"], port, self.opts["tls"] == "on")
			con.connect()
			resp = con.post(postAuthor, postGroup, postSubject, body)
			con.disconnect()
		except conn.ConnError as e:
			print("Error: %s"%(e.desc))
			return
		except Exception as e:
			print("Misc. uncaught error: %s"%e)
			return

		print(resp)
