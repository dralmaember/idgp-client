import socket, time

errcodes = [
	"000",
	"100",
	"101",
	"102",
	"200",
	"201",
	"202",
	"203",
	"204"]

TIMEOUT_SECS = 3

class ConnError(Exception):
	def __init__(self, desc):
		self.desc = desc

class Conn(object):
	def __init__(self, addr, port, tls):
		self.addr, self.port, self.tls = addr, port, tls

	def connect(self):
		self.backSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.backSock.connect((self.addr, self.port))

		if self.tls:
			raise ConnError("TLS currently not supported")
		else:
			self.conn = self.backSock

	def disconnect(self):
		if self.tls:
			pass
		self.backSock.close()

	def listGroups(self):
		self.conn.send(b"T \n")
		res = b""
		startTime = time.time()
		while time.time() <= startTime + TIMEOUT_SECS and not self.conn.fileno() == -1:
			try:
				new = self.conn.recv(1, socket.MSG_DONTWAIT)
			except OSError:
				pass
			else:
				res += new

		return res.decode("utf-8")

	def list(self, group, offset, count):
		self.conn.send(("L %s %s %s\n"%(group, offset, count)).encode("utf-8"))
		res = b""
		startTime = time.time()
		while time.time() <= startTime + TIMEOUT_SECS and not self.conn.fileno() == -1:
			try:
				new = self.conn.recv(1, socket.MSG_DONTWAIT)
			except OSError:
				pass
			else:
				res += new

		return res.decode("utf-8")

	def get(self, group, postid):
		self.conn.send(("G %s %s\n"%(group, postid)).encode("utf-8"))
		res = b""
		startTime = time.time()
		while time.time() <= startTime + TIMEOUT_SECS and not self.conn.fileno() == -1:
			try:
				new = self.conn.recv(1, socket.MSG_DONTWAIT)
			except OSError:
				pass
			else:
				res += new

		return res.decode("utf-8")

	def post(self, author, group, subject, body):
		self.conn.send(("P %s %s %s %s\n"%(author, group, len(body), subject)).encode("utf-8"))
		self.conn.send(body)
		res = b""
		startTime = time.time()
		while time.time() <= startTime + TIMEOUT_SECS and not self.conn.fileno() == -1:
			try:
				new = self.conn.recv(1, socket.MSG_DONTWAIT)
			except OSError:
				pass
			else:
				res += new

		return res.decode("utf-8")